if (sim_call_type==sim_childscriptcall_initialization) then 
    -- Put some initialization code here
    antenna=simGetObjectHandle("Transceiver_antenna")
    objectName=simGetObjectName(simGetObjectAssociatedWithScript(sim_handle_self))
    messageName="MyMessage"
    auxiliaryConsoleHandle=-1
    i=0
end 

if (sim_call_type==sim_childscriptcall_cleanup) then 
 
end 

if (sim_call_type==sim_childscriptcall_sensing) then 
    emissionRadius=simGetScriptSimulationParameter(sim_handle_self,'emissionRadius')
    if (emissionRadius<0.01) then emissionRadius=0.01 end
    
    -- Send some data:
    messageToSend="Hello World, this is my number: "..i
    simSetBoolParameter(sim_boolparam_force_show_wireless_emission,true)
    simSendData(sim_handle_all,0,messageName,messageToSend,antenna,emissionRadius)
    simSetBoolParameter(sim_boolparam_force_show_wireless_emission,false)
    i=i+1
    
    -- Receive some data (you can't receive the data that you sent!):
    while true do
        simSetBoolParameter(sim_boolparam_force_show_wireless_reception,true)
        data=simReceiveData(0,messageName,antenna)
        simSetBoolParameter(sim_boolparam_force_show_wireless_reception,false)
        if not data then
            break
        end
        if (auxiliaryConsoleHandle==-1) then
            auxiliaryConsoleHandle=simAuxiliaryConsoleOpen("Messages received by "..objectName,500,4)
        end
        simAuxiliaryConsolePrint(auxiliaryConsoleHandle,data.."\n");
    end
end 
