# README #

### Summary
This work shows an implementation and simulation of a path planning algorithm into an indoor structured environment.
This is part of the final project presentation for the Fall/2016 class "ME396D - Decision Control Human Centered Robotics".

### Version
Version 1.0

### Instrunctions

The simulation files associated with this project run used the Coppelia's VREP
robot simulation system.
Create a scene in the VREP environment and include all the lua scripts related to this project.

