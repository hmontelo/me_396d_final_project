if (sim_call_type==sim_childscriptcall_initialization) then 
    -- Put some initialization code here
    antenna=simGetObjectHandle("DirectionalTransceiver_antenna")
    objectName=simGetObjectName(simGetObjectAssociatedWithScript(sim_handle_self))
    messageName="MyMessage"
    auxiliaryConsoleHandle=-1
    i=0
end 

if (sim_call_type==sim_childscriptcall_cleanup) then 
 
end 

if (sim_call_type==sim_childscriptcall_sensing) then 
    emissionRadius=simGetScriptSimulationParameter(sim_handle_self,'emissionRadius')
    horizontalAngle=simGetScriptSimulationParameter(sim_handle_self,'horizontalAngle')
    verticalAngle=simGetScriptSimulationParameter(sim_handle_self,'verticalAngle')
    if (emissionRadius<0.01) then emissionRadius=0.01 end
    if (horizontalAngle<10) then horizontalAngle=10 end
    if (horizontalAngle>90) then horizontalAngle=90 end
    if (verticalAngle<10) then verticalAngle=10 end
    if (verticalAngle>90) then verticalAngle=90 end
    
    -- Send some data:
    messageToSend="Hello World, this is my number: "..i
    simSetBoolParameter(sim_boolparam_force_show_wireless_emission,true)
    simSendData(sim_handle_all,0,messageName,messageToSend,antenna,emissionRadius,math.pi*verticalAngle/180,math.pi*horizontalAngle/180)
    simSetBoolParameter(sim_boolparam_force_show_wireless_emission,false)
    i=i+1
    
    -- Receive some data (you can't receive the data that you sent!):
    while true do
        simSetBoolParameter(sim_boolparam_force_show_wireless_reception,true)
        data=simReceiveData(0,messageName,antenna)
        simSetBoolParameter(sim_boolparam_force_show_wireless_reception,false)
        if not data then
            break
        end
        if (auxiliaryConsoleHandle==-1) then
            auxiliaryConsoleHandle=simAuxiliaryConsoleOpen("Messages received by "..objectName,500,4)
        end
        simAuxiliaryConsolePrint(auxiliaryConsoleHandle,data.."\n");
    end
end 
